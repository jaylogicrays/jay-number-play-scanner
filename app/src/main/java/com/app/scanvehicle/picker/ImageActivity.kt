package com.app.scanvehicle.picker

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
import android.view.WindowManager
import android.widget.Toast
import com.app.scanvehicle.MainApp
import com.app.scanvehicle.permission.PermissionCheck
import com.app.scanvehicle.permission.PermissionHandler
import com.app.scanvehicle.util.Utility

class ImageActivity : Activity() {
    companion object {

        const val REQUEST_CODE_PICK_PHOTO = 1001
        const val REQUEST_CODE_CAMERA_PHOTO = 1002
        const val REQUEST_CODE_CROP_PHOTO = 1003
        const val REQUEST_CODE_PICK_DOCUMENT = 1004

        internal const val EXTRA_GALLERY = "gallery"
        internal const val EXTRA_CAMERA = "camera"
        internal const val EXTRA_FILE = "attachment"
        internal const val EXTRA_CROPPING = "crop"
        internal const val X_ASPECT_RATIO = "x_aspect_ratio"
        internal const val Y_ASPECT_RATIO = "y_aspect_ratio"
        internal var handler: PickerResultHandler? = null
        lateinit var fileUri: Uri
    }

    private var isFromGallery: Boolean = false
    private var isFromCamera: Boolean = false
    private var isAttachFile: Boolean = false
    private var isCropping: Boolean = false
    private var xAspectRatio: Int = 1
    private var yAspectRatio: Int = 1
    lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        val intent = intent
        if (intent == null || !intent.hasExtra(EXTRA_GALLERY)) {
            finish()
            return
        }
        isFromGallery = intent.getBooleanExtra(EXTRA_GALLERY, true)
        isFromCamera = intent.getBooleanExtra(EXTRA_CAMERA, true)
        isAttachFile = intent.getBooleanExtra(EXTRA_FILE, false)
        isCropping = intent.getBooleanExtra(EXTRA_CROPPING, true)
        xAspectRatio = intent.getIntExtra(X_ASPECT_RATIO, 1)
        yAspectRatio = intent.getIntExtra(Y_ASPECT_RATIO, 1)
        openSelectionDialog()
        //checkAction()
    }

    private fun checkAction() {
        if (isFromGallery && isFromCamera)
            openSelectionDialog()
        else if (isFromCamera)
            openCamera()
        else if (isFromGallery)
            openGallery()
        else
            openSelectionDialog()
    }

    private fun openSelectionDialog() {
        /*val dialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.upload_document_dialog, null)
        val tvSelectPhotoFromGallery = view.findViewById(R.id.tvSelectPhotoFromGallery) as TextView
        val tvCapturePhotoFromCamera = view.findViewById(R.id.tvCapturePhotoFromCamera) as TextView
        val tvAttachFile = view.findViewById(R.id.tvAttachFile) as TextView
        val rootLayout = view.findViewById(R.id.rootLayout) as LinearLayout
        rootLayout.setBackgroundResource(R.color.colorTransparent)

        tvSelectPhotoFromGallery.setOnClickListener {
            checkStoragePermission(0)
        }
        tvCapturePhotoFromCamera.setOnClickListener {
            checkStoragePermission(1)
        }
        tvAttachFile.setOnClickListener {
            checkStoragePermission(2)
        }

        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()*/

        val pictureDialog = AlertDialog.Builder(context)
        pictureDialog.setTitle("Select Action")
        val list = arrayListOf<CharSequence>()
        if (isFromGallery)
            list.add("Select photo from gallery")
        if (isFromCamera)
            list.add("Capture photo from camera")
        if (isAttachFile)
            list.add("Attach File")
        if (list.isNotEmpty()) {
            if (list.size == 1) {
                val which: Int = when (list[0]) {
                    "Select photo from gallery" -> {
                        0
                    }
                    "Capture photo from camera" -> {
                        1
                    }
                    else -> {
                        2
                    }
                }
                checkStoragePermission(which)
            } else {
                pictureDialog.setItems(
                    list.toTypedArray()
                ) { dialog, which ->
                    checkStoragePermission(which)
                    dialog.dismiss()
                }

                pictureDialog.setOnCancelListener {
                    finish()
                }
                pictureDialog.show()
            }
        } else {
            Toast.makeText(this, "Please provide selection type", Toast.LENGTH_SHORT).show()
            finish()
        }
    }


    private fun checkStoragePermission(which: Int) {
        PermissionCheck.check(
            context,
            Utility.getInstance().getPermissionForSDK33(),
            "",
            object : PermissionHandler() {
                override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {
                    handler!!.onFailure("Permission Denied")
                    super.onPermissionDenied(context, deniedPermissions)
                }

                override fun onGranted() {
                    when (which) {
                        0 -> {
                            openGallery()
                        }
                        1 -> {
                            checkCameraPermission()
                        }
                        2 -> {
                            openDocumentPicker()
                        }
                    }
                }
            })
    }

    private fun checkCameraPermission() {
        PermissionCheck.check(
            context,
            Manifest.permission.CAMERA,
            "",
            object : PermissionHandler() {
                override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {
                    handler!!.onFailure("Permission Denied")
                    super.onPermissionDenied(context, deniedPermissions)
                }

                override fun onGranted() {
                    openCamera()
                }
            })
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        intent.putExtra("return-data", true)
        startActivityForResult(intent, REQUEST_CODE_PICK_PHOTO)
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        fileUri = MediaUtility.getOutputMediaFileUri(context, MEDIA_TYPE_IMAGE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA_PHOTO)
    }

    private fun openDocumentPicker() {
        val intent: Intent
        if (Build.MANUFACTURER.equals("samsung", ignoreCase = true)) {
            intent = Intent("com.sec.android.app.myfiles.PICK_DATA")
            intent.putExtra("CONTENT_TYPE", "*/*")
            intent.addCategory(Intent.CATEGORY_DEFAULT)
        } else {
            val mimeTypes = arrayOf(
                "application/msword",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",  // .doc & .docx
                "application/vnd.ms-powerpoint",
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",  // .ppt & .pptx
                "application/vnd.ms-excel",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",  // .xls & .xlsx
                "text/plain",
                "application/pdf",
                "application/zip",
                "application/vnd.android.package-archive"
            )
            intent = Intent(Intent.ACTION_GET_CONTENT) // or ACTION_OPEN_DOCUMENT
            intent.type = "*/*"
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        }
        startActivityForResult(intent, REQUEST_CODE_PICK_DOCUMENT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (requestCode == REQUEST_CODE_PICK_PHOTO && resultCode == RESULT_OK && data != null && data.data != null) {
                handleGalleryResult(data)
            } else if (requestCode == REQUEST_CODE_CAMERA_PHOTO && resultCode == RESULT_OK) {
                handleCameraResult()
            } else if (requestCode == REQUEST_CODE_PICK_DOCUMENT && resultCode == RESULT_OK && data != null && data.data != null) {
                handleAttachFileResult(data)
            } else if (requestCode == REQUEST_CODE_CROP_PHOTO && resultCode == RESULT_OK) {
                handleCropImageResult()
            } else if (resultCode == RESULT_CANCELED) {
                finish()
            }
        } catch (e: Exception) {
            handler!!.onFailure(e.message!!)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleGalleryResult(data: Intent?) {
        try {
            fileUri = data!!.data!!
           getImage()
        } catch (e: Exception) {
            handler!!.onFailure(e.message!!)
        }
    }

    private fun handleCameraResult() {
        try {
            getImage()
        } catch (e: Exception) {
            handler!!.onFailure(e.message!!)
        }
    }

    private fun handleAttachFileResult(data: Intent?) {
        try {
            fileUri = data!!.data!!
            getImage()
        } catch (e: Exception) {
            handler!!.onFailure(e.message!!)
        }
    }

    private fun handleCropImageResult() {
        try {
            if (MainApp.byteArray != null) {
                val bitmap = BitmapFactory.decodeByteArray(
                    MainApp.byteArray, 0,
                    MainApp.byteArray!!.size
                )
                val actualFile = MediaUtility.from(this, fileUri)
                val actualFileName = MediaUtility.getFileNameFromUri(this, fileUri)
                handler!!.onSuccess(bitmap, actualFile, actualFileName)
            }
            getImage()
        } catch (e: Exception) {
            handler!!.onFailure(e.message!!)
        }
    }

//    private fun openCropImageActivity() {
//        val i = Intent(this@ImageActivity, CaptureImageProfile::class.java)
//            .putExtra(CaptureImageProfile.X_ASPECT_RATIO, xAspectRatio)
//            .putExtra(CaptureImageProfile.Y_ASPECT_RATIO, yAspectRatio)
//            .putExtra("imageBit", fileUri.toString())
//        startActivityForResult(i, REQUEST_CODE_CROP_PHOTO)
//    }

    private fun getImage() {
        val actualFile = MediaUtility.from(this, fileUri)
        val actualFileName = MediaUtility.getFileNameFromUri(this, fileUri)
        if (isAttachFile) {
            handler!!.onSuccess(null, actualFile, actualFileName)
        } else {
            val bitmap = BitmapFactory.decodeFile(actualFile.absolutePath)
//            val bitmap = Compressor(this).compressToBitmap(actualFile)
            handler!!.onSuccess(bitmap, actualFile, actualFileName)
        }
        finish()
    }
}

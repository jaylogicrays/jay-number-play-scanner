package com.app.scanvehicle.picker

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
import android.provider.OpenableColumns
import android.webkit.MimeTypeMap
import androidx.core.content.FileProvider
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class MediaUtility {
    companion object {
        private const val IMAGE_DIRECTORY_NAME = "IMAGE_PIC"

        fun getOutputMediaFileUri(context: Context, type: Int): Uri {
            return FileProvider.getUriForFile(
                context,
                context.applicationContext.packageName + ".fileProvider",
                getOutputMediaFile(type)!!
            )
        }

        private fun getOutputMediaFile(type: Int): File? {

            // External sdcard location
            val mediaStorageDir = File(
                Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME
            )

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null
                }
            }

            // Create a media file name
            val timeStamp = SimpleDateFormat(
                "yyyyMMdd_HH_mm_ss",
                Locale.getDefault()
            ).format(Date())
            val mediaFile: File
            if (type == MEDIA_TYPE_IMAGE) {
                mediaFile = File(
                    mediaStorageDir.path + File.separator
                            + "IMG_" + timeStamp + ".jpg"
                )
            } else {
                return null
            }

            return mediaFile
        }

        @Throws(IOException::class)
        fun from(context: Context, uri: Uri): File {
            val contentResolver = context.contentResolver
            val type =
                MimeTypeMap.getSingleton().getExtensionFromMimeType(contentResolver.getType(uri))

            val inputStream = contentResolver.openInputStream(uri)
            val tempFile = File.createTempFile("temp_store", ".$type")
            tempFile.deleteOnExit()
            var out: FileOutputStream? = null
            try {
                out = FileOutputStream(tempFile)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

            if (inputStream != null) {
                copy(inputStream, out!!)
                inputStream.close()
            }

            out?.close()
            return tempFile
        }

        @Throws(IOException::class)
        private fun copy(input: InputStream, output: OutputStream): Long {
            val eof = -1
            val defaultBufferSize = 1024 * 4

            var count: Long = 0
            var n: Int
            val buffer = ByteArray(defaultBufferSize)
            n = input.read(buffer)
            while (eof != n) {
                output.write(buffer, 0, n)
                count += n.toLong()
                n = input.read(buffer)
            }
            return count
        }

        fun getFileNameFromUri(context: Context, uri: Uri): String {
            var fileName = ""
            val cursor = context.contentResolver.query(uri, null, null, null, null)

            if (cursor != null) {
                val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (cursor.moveToFirst()) {
                    fileName = cursor.getString(nameIndex)
                }
            }
            return fileName
        }
    }
}
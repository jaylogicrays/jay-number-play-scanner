package com.app.scanvehicle.picker

import android.graphics.Bitmap
import java.io.File

abstract class PickerResultHandler {

    abstract fun onSuccess(bitmap: Bitmap?, file: File?, fileName: String)

    abstract fun onFailure(message: String)
}
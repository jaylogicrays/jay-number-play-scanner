package com.app.scanvehicle.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import com.app.scanvehicle.BaseViewBindingActivity
import com.app.scanvehicle.MainActivity
import com.app.scanvehicle.MainApp
import com.app.scanvehicle.databinding.ActivityLaunchBinding
import com.app.scanvehicle.util.Constants
import com.app.scanvehicle.util.SharedPreference
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LaunchActivity : BaseViewBindingActivity<ActivityLaunchBinding>() {

    private lateinit var binding : ActivityLaunchBinding

    override fun onCreate(instance: Bundle?, viewBinding: ActivityLaunchBinding) {
        binding = viewBinding

        Handler(Looper.getMainLooper()).postDelayed({
            val adminLevel = SharedPreference.getValue(Constants.ADMIN_LEVEL, "")

            if (adminLevel.toString().isNotEmpty()) {
                MainApp.adminLevel = adminLevel.toString()
                val intent = Intent(this,HomeActivity::class.java)
                intent.putExtra(Constants.ADMIN_LEVEL, adminLevel.toString())
                activityNav.callActivityIntent(this, intent)
            } else {
                activityNav.callActivity(this, MainActivity::class.java)
            }
            activityNav.killActivity(this)
        }, 2000)
    }

    override val bindingInflater: (LayoutInflater) -> ActivityLaunchBinding
        get() = ActivityLaunchBinding::inflate

}
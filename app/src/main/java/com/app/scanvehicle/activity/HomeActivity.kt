package com.app.scanvehicle.activity

import android.os.Bundle
import android.view.LayoutInflater
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.app.scanvehicle.BaseViewBindingActivity
import com.app.scanvehicle.MainApp
import com.app.scanvehicle.R
import com.app.scanvehicle.databinding.ActivityHomeBinding
import com.app.scanvehicle.util.Constants.ADMIN_LEVEL
import com.app.scanvehicle.util.Constants.IS_BUILDER_USER
import com.app.scanvehicle.util.Constants.IS_WATCH_MAN_USER
import com.app.scanvehicle.util.PageConfiguration
import com.app.scanvehicle.util.hideView
import com.app.scanvehicle.util.showView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseViewBindingActivity<ActivityHomeBinding>() {

    lateinit var binding: ActivityHomeBinding
    lateinit var controller: NavController
    private var adminLevel: String = ""

    override fun onCreate(instance: Bundle?, viewBinding: ActivityHomeBinding) {
        binding = viewBinding
        fetchData(instance)
        binding.bnvHome.menu.clear()
        if (MainApp.adminLevel == IS_BUILDER_USER) {
            binding.bnvHome.inflateMenu(R.menu.home_menu_builder)
        } else {
            binding.bnvHome.inflateMenu(R.menu.home_menu)
        }

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fcvHome) as NavHostFragment
        controller = navHostFragment.findNavController()
        binding.bnvHome.setupWithNavController(controller)
    }

    override val bindingInflater: (LayoutInflater) -> ActivityHomeBinding
        get() = ActivityHomeBinding::inflate

    private fun fetchData(savedInstanceState: Bundle?) {
        val extras = intent.extras
        if (extras != null) {
            adminLevel = extras.getString(ADMIN_LEVEL,"")
        }
        savedInstanceState?.let {
            adminLevel = it.getString(ADMIN_LEVEL, "")
        }
    }

    fun onDestinationChanged(destination: NavDestination) {
        val config = PageConfiguration.gerConfiguration(destination.id)
        if (config.bottomNavigationBarVisible) {
            if (adminLevel == IS_WATCH_MAN_USER) {
                binding.bnvHome.hideView()
            } else {
                binding.bnvHome.showView()
            }
        } else {
            binding.bnvHome.hideView()
        }
    }

}
package com.app.scanvehicle.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.widget.Toast
import androidx.activity.viewModels
import com.app.scanvehicle.BaseViewBindingActivity
import com.app.scanvehicle.MainApp
import com.app.scanvehicle.R
import com.app.scanvehicle.api.NetworkResult
import com.app.scanvehicle.databinding.ActivitySignInBinding
import com.app.scanvehicle.model.UserModel
import com.app.scanvehicle.util.Constants.ADMIN_LEVEL
import com.app.scanvehicle.util.Constants.USER_INFO
import com.app.scanvehicle.util.SharedPreference
import com.app.scanvehicle.util.checkPermissionReadWrite
import com.app.scanvehicle.util.hideKeyboard
import com.app.scanvehicle.util.hideView
import com.app.scanvehicle.util.showView
import com.app.scanvehicle.viewModel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInActivity : BaseViewBindingActivity<ActivitySignInBinding>() {

    private val viewModel: LoginViewModel by viewModels()
    private lateinit var userData: UserModel
    private lateinit var binding: ActivitySignInBinding

    override fun onCreate(instance: Bundle?, viewBinding: ActivitySignInBinding) {
        binding = viewBinding
        binding.btnLogin.setOnClickListener {
            if (isCheckValidation()) {
                callLoginApi()
            }
            /*            if (binding.etUserEmail.text!!.trim()
                                .toString() == "admin@gmail.com" && binding.etPassword.text!!.trim()
                                .toString() == "admin"
                        ) {
                            checkWatchManUser(false)

                        } else if (binding.etUserEmail.text!!.trim()
                                .toString() == "watchman@gmail.com" && binding.etPassword.text!!.trim()
                                .toString() == "admin"
                        ) {
                            checkWatchManUser(true)

                        } else {
                            Toast.makeText(this, "Email & password is wrong", Toast.LENGTH_SHORT).show()
                        }*/
        }
    }

    override val bindingInflater: (LayoutInflater) -> ActivitySignInBinding
        get() = ActivitySignInBinding::inflate

    private fun callLoginApi() {
        viewModel.loginUser(
            binding.etUserEmail.text!!.trim().toString(),
            binding.etPassword.text!!.trim().toString()
        )
        viewModel.loginResponse.observe(this) {
            when (it) {
                is NetworkResult.Connection -> {
                    loader.hide()
                    showNoInternetDialog {
                        viewModel.loginUser(
                            binding.etUserEmail.text!!.trim().toString(),
                            binding.etPassword.text!!.trim().toString()
                        )
                    }
                    loader.hide()
                }

                is NetworkResult.Error -> {
                    loader.hide()
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_SHORT).show()
                    Log.d("MyValue", "Error Message:: ${it.errorMessage}")
                    loader.hide()
                }

                is NetworkResult.Loading -> {
                    loader.showDialog(this)
                    Log.d("MyValue", "Loading...")
                }

                is NetworkResult.Success -> {
                    userData = it.data
                    Log.d("MyValue", "Success :: ${it.data}")
                    checkWatchManUser(it.data)
                    loader.hide()
                }
            }
        }
    }

    private fun checkWatchManUser(adminLevel: UserModel) {
        SharedPreference.setValue(ADMIN_LEVEL, adminLevel.admin_level)
        SharedPreference.setValue(USER_INFO, adminLevel)
        MainApp.adminLevel = adminLevel.admin_level
        hideKeyboard(this)
        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra(ADMIN_LEVEL, userData.admin_level)
        this.checkPermissionReadWrite {
            activityNav.callActivityIntent(this, intent)
            activityNav.killActivity(this)
        }
    }

    private fun isCheckValidation(): Boolean {
        var isValid = true
        val email = binding.etUserEmail.text!!.trim().toString()
        val password = binding.etPassword.text!!.trim().toString()

        val isEmailValid = email.isNotBlank() && (Patterns.EMAIL_ADDRESS.matcher(email)
            .matches() || TextUtils.isDigitsOnly(email))
        val isPasswordValid = password.length >= 8

        if (email.isBlank()) {
            binding.tvErrorEmail.showView()
            binding.tvErrorEmail.text = getString(R.string.str_please_enter_email_id)
            isValid = false
        } else if (!isEmailValid) {
            binding.tvErrorEmail.showView()
            binding.tvErrorEmail.text = getString(R.string.validation_email_pattern)
            isValid = false
        } else {
            binding.tvErrorEmail.hideView()
        }

        if (password.isBlank()) {
            binding.tvErrorPassword.showView()
            binding.tvErrorPassword.text = getString(R.string.str_please_enter_password)
            isValid = false
        } else if (!isPasswordValid) {
            binding.tvErrorPassword.showView()
            binding.tvErrorPassword.text = getString(R.string.str_password_must_contain_8_characters)
            isValid = false
        } else {
            binding.tvErrorPassword.hideView()
        }
        return isValid
    }
}
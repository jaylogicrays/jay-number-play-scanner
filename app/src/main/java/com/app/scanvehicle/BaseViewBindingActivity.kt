package com.app.scanvehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.app.scanvehicle.custom.CustomDialog
import com.app.scanvehicle.util.ActivityNav
import com.app.scanvehicle.util.Generics
import com.app.scanvehicle.util.SharedPreference
import com.app.scanvehicle.util.Utility
import com.app.scanvehicle.util.alertDialog
import javax.inject.Inject

abstract class BaseViewBindingActivity<VB : ViewBinding> : AppCompatActivity() {

    private var binding: ViewBinding? = null

    @Inject
    lateinit var utility: Utility

    @Inject
    lateinit var loader: CustomDialog

    @Inject
    lateinit var genericsFun: Generics

    @Inject
    lateinit var activityNav: ActivityNav

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = bindingInflater.invoke(layoutInflater)
        setContentView(binding!!.root)
        onCreate(savedInstanceState, binding as VB)
    }

    /**
     * @method Show No Internet Screen/Dialog when is internet/wifi is not connect
     * @param tryAgainClick = Call function, methods on Try Again button click
     */
    fun showNoInternetDialog( tryAgainClick: View.OnClickListener) {
        utility.showNoInternetDialog(this) {
            tryAgainClick.onClick(it)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    protected abstract fun onCreate(instance: Bundle?, viewBinding: VB)

    protected abstract val bindingInflater: (LayoutInflater) -> VB

//    fun activityContext(): HomeActivity {
//        return (activity as HomeActivity)
//    }

    fun userLogout() {
        this.alertDialog(getString(R.string.str_logout), getString(R.string.str_logout_title_bottom_sheet)) {
        SharedPreference.clearPreferences()

        activityNav.callActivity(this, MainActivity::class.java)
        activityNav.killActivity(this)
        }
    }
}
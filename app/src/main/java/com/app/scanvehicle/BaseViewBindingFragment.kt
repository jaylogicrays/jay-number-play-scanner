package com.app.scanvehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.app.scanvehicle.activity.HomeActivity
import com.app.scanvehicle.custom.CustomDialog
import com.app.scanvehicle.util.ActivityNav
import com.app.scanvehicle.util.Generics
import com.app.scanvehicle.util.SharedPreference
import com.app.scanvehicle.util.Utility
import com.app.scanvehicle.util.alertDialog
import javax.inject.Inject

abstract class BaseViewBindingFragment<VB : ViewBinding> : Fragment() {

    private var binding: ViewBinding? = null

    @Inject
    lateinit var utility: Utility

    @Inject
    lateinit var loader: CustomDialog

    @Inject
    lateinit var genericsFun: Generics

    @Inject
    lateinit var activityNav: ActivityNav

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = bindingInflater.invoke(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreated(savedInstanceState, binding as VB)
    }

    /**
     * @method Show No Internet Screen/Dialog when is internet/wifi is not connect
     * @param tryAgainClick = Call function, methods on Try Again button click
     */
    fun showNoInternetDialog( tryAgainClick: View.OnClickListener) {
        utility.showNoInternetDialog(requireContext()) {
            tryAgainClick.onClick(it)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    protected abstract fun onViewCreated(instance: Bundle?, viewBinding: VB)

    protected abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> VB

    fun activityContext(): HomeActivity {
        return (activity as HomeActivity)
    }

    fun userLogout() {
        activityContext().alertDialog(getString(R.string.str_logout),
            getString(R.string.str_logout_title_bottom_sheet)) {
        SharedPreference.clearPreferences()
        activityNav.callActivity(requireActivity(), MainActivity::class.java)
        activityNav.killActivity(requireActivity())
        }
    }

}
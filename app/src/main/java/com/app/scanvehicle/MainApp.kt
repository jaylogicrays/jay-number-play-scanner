package com.app.scanvehicle

import android.app.Application
import androidx.lifecycle.LiveData
import com.app.scanvehicle.util.CheckInternetConnection
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApp : Application() {

    private lateinit var cld: LiveData<Boolean>
    private var isConnected: Boolean = false

    companion object {
        var adminLevel : String = ""
        var enterAnimationFragment: Int = 0
        var exitAnimationFragment: Int = 0
        var enterAnimationActivity: Int = 0
        var exitAnimationActivity: Int = 0
        lateinit var appInstance: MainApp
        var byteArray: ByteArray? = null

        @Synchronized
        fun getInstance(): MainApp {
            return appInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        cld = CheckInternetConnection(this)
        observeConnectionStatus()
    }

    fun isConnectionAvailable(): Boolean {
        return isConnected
    }

    private fun observeConnectionStatus() {
        cld.observeForever { t ->
            isConnected = t!!
        }
    }
}
package com.app.scanvehicle.repository

import com.app.scanvehicle.api.ApiServer
import com.app.scanvehicle.api.NetworkResult
import com.app.scanvehicle.model.VehicleData
import com.app.scanvehicle.model.VehicleDataItem
import com.app.scanvehicle.roomdb.VehicleDao
import com.app.scanvehicle.util.Constants.GENERAL_SETTING
import com.app.scanvehicle.util.SharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.io.IOException
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiServer: ApiServer, private val vehicleDao: VehicleDao ) : BaseApiResponse() {

    /**
     * Call Vehicle info API
     */
    suspend fun getVehicle() = flow<NetworkResult<VehicleData>> {
        emit(NetworkResult.Loading(true))
        if (apiServer.getVehicleData().body() != null) {
            val data = apiServer.getVehicleData().body()!!
            emit(safeApiCall { apiServer.getVehicleData() })

            vehicleDao.deleteAllVehicleData()   //Delete table from Room
            SharedPreference.deletePreference(GENERAL_SETTING) //Delete general setting Preference
            SharedPreference.setValue(
                GENERAL_SETTING,
                data.general_setting.show_person_name
            ) //Store general setting Preference
            vehicleDao.insertVehicleDataItem(data.vehicle_data) // Insert vehicle data in Room
        }
    }.catch { e ->
        if (e is IOException) {
            emit(
                NetworkResult.Connection(
                    "No internet connection, please check your network connection", true
                )
            )
        } else
            emit(NetworkResult.Error(e.message ?: "Unknown Error"))
    }.flowOn(Dispatchers.IO)

    /*      emit(NetworkResult.Loading(true))
            val response = apiServer.getVehicleData()
            emit(NetworkResult.Success(response))*/

    /**
     * Get Vehicle data From Room Database
     */
    fun getVehicleFromDB(): List<VehicleDataItem> = vehicleDao.getVehicleDataItem()

    fun deleteVehicleData() = vehicleDao.deleteAllVehicleData()

    /**
     *  Call Login API
     */
    suspend fun login(email: String, password: String) = flow {
        emit(NetworkResult.Loading(true))
        val response = apiServer.login(email, password)
        if (response.response) {
            emit(NetworkResult.Success(response))
        } else {
            emit(NetworkResult.Error(response.msg))
        }
    }.catch { e ->
        if (e is IOException) {
            emit(
                NetworkResult.Connection(
                    "No internet connection, please check your network connection", true
                )
            )
        } else
            emit(NetworkResult.Error(e.message ?: "Unknown Error"))
    }.flowOn(Dispatchers.IO)

}
package com.app.scanvehicle.viewModel

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.scanvehicle.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel  @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    val mutableProfileImage : MutableLiveData<Bitmap?> = MutableLiveData()

}
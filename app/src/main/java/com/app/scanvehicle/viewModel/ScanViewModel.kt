package com.app.scanvehicle.viewModel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.scanvehicle.api.NetworkResult
import com.app.scanvehicle.databinding.FragmentScanNumberBinding
import com.app.scanvehicle.model.VehicleData
import com.app.scanvehicle.model.VehicleDataItem
import com.app.scanvehicle.repository.MainRepository
import com.app.scanvehicle.util.dialContactPhone
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ScanViewModel @Inject constructor(private val mainRepository: MainRepository)  : ViewModel() {

    private val _getVehicleResponse = MutableLiveData<NetworkResult<VehicleData>>()

    val getVehicleResponse: LiveData<NetworkResult<VehicleData>> = _getVehicleResponse
    var binding : FragmentScanNumberBinding? = null
    init {
        getVehicleData()
    }

    /**
     * Call Vehicle Api
     */
    fun getVehicleData() {
        viewModelScope.launch {
            mainRepository.getVehicle().collectLatest { response ->
                Log.d("MyValue", "getVehicleData: $response")
                _getVehicleResponse.postValue(response)
            }
        }
    }

    /**
     * get Vehicle data from Room Database
     */
    fun getVehicleFromDB(): List<VehicleDataItem> = mainRepository.getVehicleFromDB()

    /**
     * Call Delete vehicle Data from Room Database
     */
    fun deleteVehicleDB() = mainRepository.deleteVehicleData()


    /**
     * set Default String on textview filed
     */
    fun setBlankDataOnWidget(context: Context) {
        callNowClickListener(context,"")
        binding!!.tvName.text = "Name"
        binding!!.tvVehicleNo.text = "GJ-XX-XX-XXXX"
        binding!!.tvVehicleType.text = "X Wheeler"
        binding!!.tvOfficeName.text = "-- -- --"
        binding!!.tvOfficeNo.text = "00 - 00"
    }

    /**
     * Call to given number
     * @param phoneNumber this is use for Contact Number to Person/Company
     */
    private fun callNowClickListener(context: Context, phoneNumber: String) {
        binding!!.btnCallNow.setOnClickListener {
            if (phoneNumber.isNotEmpty())
                context.dialContactPhone(phoneNumber)
        }
    }

    /**
     * set Data on UI like text view
     * @param context this is Fragment Context
     * @param vehicleDataItem this a list of vehicle info
     * @param generalSetting this is api value 'show_person_name' it is use for hide/show person name
     */
    fun setVehicleDataOnUI(context: Context, vehicleDataItem: VehicleDataItem?,
                           generalSetting: String ) {

        Log.d("MyValue", "vehicleDataItem: $vehicleDataItem")
        vehicleDataItem?.let {
            binding!!.tvName.apply {
                text = if (generalSetting == "1") {
                    it.person_name
                } else "${it.person_name[0]}XXXX"
            }
            /** Phone Call to company */
            callNowClickListener(context, it.company_mob)
            binding!!.tvVehicleNo.text = it.vehicle_no
            binding!!.tvVehicleType.text = it.vehicle_type
            binding!!.tvOfficeName.text = it.company_name
            binding!!.tvOfficeNo.text = it.office_no
        }
    }
}
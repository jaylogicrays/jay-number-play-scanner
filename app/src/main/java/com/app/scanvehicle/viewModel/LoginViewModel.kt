package com.app.scanvehicle.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.scanvehicle.repository.MainRepository
import com.app.scanvehicle.api.NetworkResult
import com.app.scanvehicle.model.UserModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val mainRepository: MainRepository): ViewModel() {

    private val _loginResponse = MutableLiveData<NetworkResult<UserModel>>()

    val loginResponse : LiveData<NetworkResult<UserModel>> = _loginResponse

    fun loginUser(gmail : String, password: String) {
        viewModelScope.launch {
            mainRepository.login(gmail, password).collectLatest { response->
                Log.d("MyValue", "getVehicleData: $response")
                _loginResponse.postValue(response)
            }
        }
    }
}
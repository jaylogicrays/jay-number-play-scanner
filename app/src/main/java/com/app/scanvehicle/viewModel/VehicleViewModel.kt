package com.app.scanvehicle.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.scanvehicle.repository.MainRepository
import com.app.scanvehicle.api.NetworkResult
import com.app.scanvehicle.model.VehicleData
import com.app.scanvehicle.model.VehicleDataItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VehicleViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    private val _getVehicleResponse = MutableLiveData<NetworkResult<VehicleData>>()

    val getVehicleResponse: LiveData<NetworkResult<VehicleData>> = _getVehicleResponse

    init {
        getVehicleData()
    }

    /**
     * Call Vehicle Api
     */
    fun getVehicleData() {
        viewModelScope.launch {
            mainRepository.getVehicle().collectLatest { response ->
                Log.d("MyValue", "getVehicleData: $response")
                _getVehicleResponse.postValue(response)
            }
        }
    }

    /**
     * get Vehicle data from Room Database
     */
    fun getVehicleFromDB(): List<VehicleDataItem> = mainRepository.getVehicleFromDB()

}
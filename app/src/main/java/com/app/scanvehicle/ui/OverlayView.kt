package com.app.scanvehicle.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View

class OverlayView (context: Context, attrs: AttributeSet?) : View(context, attrs) {
    private val rectangles = mutableListOf<Rect>()

    fun addRectangle(rect: Rect) {
        rectangles.add(rect)
        postInvalidate()
    }

    fun clearRectangles() {
        rectangles.clear()
        postInvalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val paint = Paint()
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 5f

        for (rect in rectangles) {
            canvas.drawRect(rect, paint)
        }
    }
}
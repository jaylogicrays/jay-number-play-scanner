package com.app.scanvehicle

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import com.app.scanvehicle.activity.SignInActivity
import com.app.scanvehicle.databinding.ActivityMainBinding
import com.app.scanvehicle.slidetoact.SlideToActView
import com.app.scanvehicle.util.hideView

class MainActivity : BaseViewBindingActivity<ActivityMainBinding>() {

    private lateinit var binding: ActivityMainBinding

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        get() = ActivityMainBinding::inflate

    override fun onCreate(instance: Bundle?, viewBinding: ActivityMainBinding) {
        binding = viewBinding

        binding.btnSlideActView.onSlideToActAnimationEventListener =
            object : SlideToActView.OnSlideToActAnimationEventListener {
                override fun onSlideCompleteAnimationStarted(
                    view: SlideToActView,
                    threshold: Float
                ) {
                    binding.btnGetStart.hideView()
                    binding.ivArrow.hideView()
                }

                override fun onSlideCompleteAnimationEnded(view: SlideToActView) {}

                override fun onSlideResetAnimationStarted(view: SlideToActView) {}

                override fun onSlideResetAnimationEnded(view: SlideToActView) {}
            }

        binding.btnSlideActView.onSlideCompleteListener =
            object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    startActivity(Intent(this@MainActivity, SignInActivity::class.java))
                    this@MainActivity.finish()
                }
            }
    }

}
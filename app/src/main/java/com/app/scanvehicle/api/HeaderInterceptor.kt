package com.app.scanvehicle.api

import okhttp3.Interceptor
import okhttp3.Response

internal class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
            .addHeader("Accept", "application/json")
//        if (authToken.isNotEmpty()) {
//            builder.addHeader("Authorization", "Bearer $authToken")
//        }
        return chain.proceed(builder.build())
    }
}
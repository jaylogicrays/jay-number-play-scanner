package com.app.scanvehicle.api

import com.app.scanvehicle.model.UserModel
import com.app.scanvehicle.model.VehicleData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServer {

 @GET("getVehicleData.php")
 suspend fun getVehicleData() : Response<VehicleData>

// @POST("getVehicleData.php?func=getLoginData&email=sanjay@logicrays.com&passwd=a4ma4m!!")
 @POST("getVehicleData.php?func=getLoginData")
 suspend fun login(@Query(RequestParams.Email) email : String, @Query(RequestParams.password) password : String ) : UserModel

}
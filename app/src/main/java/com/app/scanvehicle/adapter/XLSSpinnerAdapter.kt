package com.app.scanvehicle.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.marginRight
import com.app.scanvehicle.R
import com.app.scanvehicle.util.hideView
import com.app.scanvehicle.util.showView

class XLSSpinnerAdapter (private val ctx: Context, resource: Int,
                         private val contentArray: Array<String>, private val imageArray: Array<Int> ) :

    ArrayAdapter<String?>(ctx, R.layout.spinner_img_txt_layout, R.id.spinnerTextView, contentArray) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.spinner_img_txt_layout, null)
        }

//        val textView = convertView!!.findViewById<TextView>(R.id.spinnerTextView)
//        textView.text = contentArray[position]
        val imageView = convertView!!.findViewById<ImageView>(R.id.spinnerImages)
//        if (position == 0)
//            imageView.hideView()
//        else
//            imageView.showView()

        imageView.setImageResource(imageArray[position])
        return convertView
    }
}
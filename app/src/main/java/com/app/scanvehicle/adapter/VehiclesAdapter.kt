package com.app.scanvehicle.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.scanvehicle.databinding.ItemVehiclelistBinding
import com.app.scanvehicle.model.VehicleDataItem

class VehiclesAdapter() : ListAdapter<VehicleDataItem, VehiclesAdapter.MyViewHolder>(MyDiffUtill()) {

    lateinit var ctn : Context
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
       val view = ItemVehiclelistBinding.inflate(LayoutInflater.from(parent.context), parent,false)
        ctn = parent.context
        return MyViewHolder(view)
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onBindViewHolder(holder: VehiclesAdapter.MyViewHolder, position: Int) {

//        holder.binding.vehicleDataItem = currentList[position]
        holder.binding.tvName.text = currentList[position].person_name
        holder.binding.tvVehicleNo.text = currentList[position].vehicle_no
    }

    class MyViewHolder(myBinding: ItemVehiclelistBinding) : RecyclerView.ViewHolder(myBinding.root){
        val binding = myBinding
    }

    class MyDiffUtill : DiffUtil.ItemCallback<VehicleDataItem>(){
        override fun areItemsTheSame(oldItem: VehicleDataItem, newItem: VehicleDataItem): Boolean {
           return oldItem.id_vehicle == newItem.id_vehicle
        }

        override fun areContentsTheSame(oldItem: VehicleDataItem, newItem: VehicleDataItem): Boolean {
            return oldItem == newItem
        }

    }
}
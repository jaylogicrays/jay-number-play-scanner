package com.app.scanvehicle.custom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.app.scanvehicle.R
import com.app.scanvehicle.model.SpinnerModel


class CustomSpinnerAdapter(
    context: Context,
    private val arrayList: ArrayList<SpinnerModel>
) : ArrayAdapter<SpinnerModel>(context, 0) {

    //private var arrayList: Array<SpinnerModel> = objects
    //private var mContext: Context? = context

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getCount(): Int {
        return arrayList.size
    }

    private fun getCustomView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context)
            .inflate(R.layout.spinner_item, parent, false)
        val label = view.findViewById<View>(R.id.tvSpinnerObject) as TextView
        var textColor = R.color.colorBlack
        if (position == 0) {
            textColor = R.color.colorGray
        }
        label.setTextColor(ContextCompat.getColor(context, textColor))
        label.text = arrayList[position].name
        return view
    }
}



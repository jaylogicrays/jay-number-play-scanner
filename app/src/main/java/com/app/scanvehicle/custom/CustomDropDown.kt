package com.app.scanvehicle.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import androidx.databinding.ObservableInt
import com.app.scanvehicle.R
import com.app.scanvehicle.model.SpinnerModel

class CustomDropDown : LinearLayout {

    private lateinit var tvTitle: TextView
    private lateinit var tvError: TextView
    lateinit var spinner: Spinner
    private var emptyValidateText: String? = null
    var selectedId: ObservableInt = ObservableInt(0)
    var selectedValue = ""

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        inIt(context, attrs, -1)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context, attrs, defStyleAttr
    ) {

        inIt(context, attrs, defStyleAttr)
    }

    private fun inIt(context: Context, attrs: AttributeSet, defStyleAttr: Int) {
        val itemView: View =
            LayoutInflater.from(context).inflate(R.layout.custom_drop_down_layout, this, true)
        tvTitle = itemView.findViewById(R.id.tvTitle)
        tvError = itemView.findViewById(R.id.tvError)
        spinner = itemView.findViewById(R.id.spinner)

        attrs.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.CustomDropDown, 0, 0)
            val value = typedArray.getString(R.styleable.CustomDropDown_dropDownValueText)
            val enable = typedArray.getBoolean(R.styleable.CustomEditText_android_enabled, true)
            if (typedArray.getString(R.styleable.CustomDropDown_dropDownEmptyValidateText) != null)
                emptyValidateText =
                    typedArray.getString(R.styleable.CustomDropDown_dropDownEmptyValidateText)
            val firstIndexValue =
                typedArray.getString(R.styleable.CustomDropDown_dropDownFirstIndexValue)
            tvTitle.text = value
            spinner.isEnabled = enable
            //val entries: Array<CharSequence> = typedArray.getTextArray(R.styleable.CustomDropDown_android_entries)
            val userTypeList: ArrayList<SpinnerModel> = arrayListOf()
            userTypeList.add(SpinnerModel(0, firstIndexValue!!))
            setSpinnerItems(userTypeList, 0)
            selectedValue = userTypeList[0].name
            typedArray.recycle()
        }
    }

    fun setSpinnerItems(list: ArrayList<SpinnerModel>, selectedPos: Int) {
        spinner.adapter = CustomSpinnerAdapter(context, list)
        spinner.setSelection(selectedPos)
        spinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    selectedValue = list[p0!!.selectedItemPosition].name
                    selectedId.set(list[p0.selectedItemPosition].id)
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
    }

    fun setEnable(enabled: Boolean) {
        spinner.isEnabled = enabled
    }

    fun isValidate(): Boolean {
        return if (spinner.selectedItemPosition == 0) {
            tvError.text = emptyValidateText
            tvError.visibility = View.VISIBLE
            false
        } else {
            tvError.text = ""
            tvError.visibility = View.GONE
            true
        }
    }
}
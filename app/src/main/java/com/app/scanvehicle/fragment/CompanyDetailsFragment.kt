package com.app.scanvehicle.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.app.scanvehicle.BaseViewBindingFragment
import com.app.scanvehicle.databinding.FragmentCompanyDetailsBinding

class CompanyDetailsFragment : BaseViewBindingFragment<FragmentCompanyDetailsBinding>() {

    lateinit var binding: FragmentCompanyDetailsBinding

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentCompanyDetailsBinding) {
        binding = viewBinding
        findNavController().addOnDestinationChangedListener { _,destination,_ ->
            if (activity != null)
                activityContext().onDestinationChanged(destination)
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentCompanyDetailsBinding
        get() = FragmentCompanyDetailsBinding::inflate

}
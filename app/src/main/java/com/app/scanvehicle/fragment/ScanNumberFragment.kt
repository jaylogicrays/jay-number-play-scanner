package com.app.scanvehicle.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.app.scanvehicle.BaseViewBindingFragment
import com.app.scanvehicle.activity.HomeActivity
import com.app.scanvehicle.api.NetworkResult
import com.app.scanvehicle.databinding.FragmentScanNumberBinding
import com.app.scanvehicle.model.VehicleDataItem
import com.app.scanvehicle.util.Constants.GENERAL_SETTING
import com.app.scanvehicle.util.SharedPreference
import com.app.scanvehicle.util.hideKeyboard
import com.app.scanvehicle.util.hideView
import com.app.scanvehicle.util.showToast
import com.app.scanvehicle.util.showView
import com.app.scanvehicle.viewModel.ScanViewModel
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.Locale

private const val TAG = "ScanFragment"

@AndroidEntryPoint
class ScanNumberFragment : BaseViewBindingFragment<FragmentScanNumberBinding>() {

    private val viewModel: ScanViewModel by viewModels()

    lateinit var binding: FragmentScanNumberBinding
    private val regex = Regex("[^A-Za-z0-9]")

    private var vehicleData = ArrayList<VehicleDataItem>()
    lateinit var mCameraSource: CameraSource
    private var generalSetting: String = ""

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentScanNumberBinding) {
        binding = viewBinding
        viewModel.binding = viewBinding
        callVehicleApi()
        startCameraSource()
        viewModel.setBlankDataOnWidget(requireContext())
        logout()

        findNavController().addOnDestinationChangedListener { _, destination, _ ->
            if (activity != null)
                activityContext().onDestinationChanged(destination)
        }
    }

    private fun logout() {
        binding.imgBtnLogout.setOnClickListener {
            viewModel.deleteVehicleDB()
            userLogout()
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentScanNumberBinding
        get() = FragmentScanNumberBinding::inflate

    private fun startCameraSource() {
        //Create the TextRecognizer
        val textRecognizer: TextRecognizer =
            TextRecognizer.Builder(activity!!.applicationContext).build()
        if (!textRecognizer.isOperational) {
            Log.d(TAG, "Detector dependencies not loaded yet")
        } else {
            //Initialize camera source to use high resolution and set Autofocus on.
            mCameraSource = CameraSource.Builder(activity!!.applicationContext, textRecognizer)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1280, 1024)
                .setAutoFocusEnabled(true)
                .setRequestedFps(2.0f)
                .build()

            /**
             * Add call back to SurfaceView and check if camera permission is granted.
             * If permission is granted we can start our cameraSource and pass it to surfaceView
             */
            binding.surfaceCameraView.holder.addCallback(object : SurfaceHolder.Callback {
                override fun surfaceCreated(holder: SurfaceHolder) {
                    try {
                        if (activity?.let {
                                ActivityCompat.checkSelfPermission(it.applicationContext, Manifest.permission.CAMERA)
                            } != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(activity as HomeActivity,
                                arrayOf(Manifest.permission.CAMERA), 101)
                            return
                        } else
                            mCameraSource.start(binding.surfaceCameraView.holder)

                        /**
                         * Manual Start button Listener: Show Manual layout and search vehicle no
                         */
                        binding.btnManually.setOnClickListener {
                            mCameraSource.stop()
                            binding.clManual.showView()
                            binding.btnManually.hideView()
                            viewModel.setBlankDataOnWidget(requireContext())
                            checkNoManually()
                        }

                        /**
                         * Manual Cancel button Listener: Hide Manual layout
                         */
                        binding.btnCancel.setOnClickListener {
                            hideKeyboard(activity as HomeActivity)
                            viewModel.setBlankDataOnWidget(requireContext())
                            binding.clManual.hideView()
                            binding.btnManually.showView()
                            mCameraSource.start(binding.surfaceCameraView.holder)
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
                }

                /**
                 * Release resources for cameraSource
                 */
                override fun surfaceDestroyed(holder: SurfaceHolder) {
                    mCameraSource.stop()
                }
            })

            //Set the TextRecognizer's Processor.
            textRecognizer.setProcessor(object : Detector.Processor<TextBlock> {
                override fun release() {}

                /**
                 * Detect all the text from camera using TextBlock and the values into a stringBuilder
                 * which will then be set to the textView.
                 */
                override fun receiveDetections(detections: Detector.Detections<TextBlock?>) {
                    val items: SparseArray<TextBlock?> = detections.detectedItems
                    var result = ""
                    if (items.size() != 0) {
                        activity!!.runOnUiThread {
                            val stringBuilder = StringBuilder()
                            for (i in 0 until items.size()) {
                                val item = items.valueAt(i)

/*                                val textBlockFrame = item?.boundingBox
                                if (isTextBlockInsideScanFrame(textBlockFrame)) {
                                    // Text block is entirely within the scanFrame, process it
                                    val text = item?.value
                                    requireContext().showToast(":: $text")
                                    Log.d(TAG, "checkVehicle Result: $result")
                                }*/

                                stringBuilder.append(item?.value)
                                stringBuilder.append("\n")
                                result = regex.replace(stringBuilder, "")
                            }
                            if (result.length >=4)
                                checkVehicleByScan(result)
                        }
                    }
                }
            })
        }
    }

    private fun checkNoManually() {
        binding.btnSearch.setOnClickListener {
            binding.etManual.text.toString().trim().let {
                if (it.isNotEmpty()) {
                    if (it.length >= 4) {
                        val vehicleNum = regex.replace(it.uppercase(Locale.getDefault()), "")
                        checkVehicle(vehicleNum)
                    } else {
                        binding.etManual.error = "Please enter valid vehicle number"
                    }
                } else {
                    binding.etManual.error = "Please enter the vehicle number"
                }
            }
        }
    }


    private fun checkVehicleByScan(vehicleNo: String) {
        for (i in vehicleData.indices) {
            val vehicleNoData = regex.replace(vehicleData[i].vehicle_no, "")
            if (vehicleNo.contains(vehicleNoData)) {
                viewModel.setVehicleDataOnUI(requireContext(), vehicleData[i], generalSetting)
                break
            } else {
                viewModel.setBlankDataOnWidget(requireContext())
            }
        }
    }

    private fun checkVehicle(vehicleNo: String) {
        for (i in vehicleData.indices) {
            val vehicleNoData = regex.replace(vehicleData[i].vehicle_no, "")
            if (vehicleNoData.contains(vehicleNo)) {
                viewModel.setVehicleDataOnUI(requireContext(), vehicleData[i], generalSetting)
                break
            } else {
                viewModel.setBlankDataOnWidget(requireContext())
            }
        }
    }

    private fun callVehicleApi() {
        viewModel.getVehicleResponse.observe(this) {
            when (it) {
                is NetworkResult.Connection -> {
                    val data = SharedPreference.getValue(GENERAL_SETTING, "")
                    if (data == "") {
                        showNoInternetDialog { viewModel.getVehicleData() }
                    } else {
                        generalSetting = data.toString()
                        getVehicleFromDB()
                    }
                }

                is NetworkResult.Error -> {
                    requireContext().showToast(it.errorMessage)
                    loader.hide()
                    Log.d("MyValue", "Error Message:: ${it.errorMessage}")
                }

                is NetworkResult.Loading -> {
                    loader.showDialog(activity)
                    Log.d("MyValue", "Loading...")
                }

                is NetworkResult.Success -> {
                    loader.hide()
                    vehicleData.clear()
                    generalSetting = it.data.general_setting.show_person_name
                    vehicleData.addAll(it.data.vehicle_data)
                    Log.d("MyValue", "Success :: ${it.data}")
                }
            }
        }
    }

    private fun getVehicleFromDB() {
        vehicleData.clear()
        lifecycleScope.launch {
            viewModel.getVehicleFromDB().forEach {
                Log.d("MyDBValue", "getVehicleFromDB: $it")
                vehicleData.addAll(listOf(it))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mCameraSource.release()
        mCameraSource.stop()
    }

    /*    private fun isTextBlockInsideScanFrame(textBlockFrame: Rect?): Boolean {
        textBlockFrame?.let {
            return it.left >= binding.scanFrameOverlay.left &&
                    it.top >= binding.scanFrameOverlay.top &&
                    it.right <= binding.scanFrameOverlay.right &&
                    it.bottom <= binding.scanFrameOverlay.bottom
        }
        return false
    }*/

}
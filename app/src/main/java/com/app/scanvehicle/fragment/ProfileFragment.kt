package com.app.scanvehicle.fragment

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.app.scanvehicle.BaseViewBindingFragment
import com.app.scanvehicle.MainApp
import com.app.scanvehicle.R
import com.app.scanvehicle.databinding.FragmentProfileBinding
import com.app.scanvehicle.picker.MediaPicker
import com.app.scanvehicle.picker.PickerResultHandler
import com.app.scanvehicle.viewModel.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.File

@AndroidEntryPoint
class ProfileFragment : BaseViewBindingFragment<FragmentProfileBinding>(), View.OnClickListener {

    lateinit var binding: FragmentProfileBinding

    private val viewModel: ProfileViewModel by viewModels()


    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentProfileBinding) {
        binding = viewBinding
        binding.clickListener = this
        binding.adminLevel = MainApp.adminLevel
        binding.lifecycleOwner = this

        findNavController().addOnDestinationChangedListener { _, destination,_ ->
            if (activity != null)
                activityContext().onDestinationChanged(destination)
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentProfileBinding
        get() = FragmentProfileBinding::inflate

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.cvProfile -> {
               openImagePicker()
            }
            R.id.btnChange -> {
                findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToChangePasswordDialogFragment())
            }
        }
    }

    private fun openImagePicker() {
        MediaPicker.getImageFromMedia(requireContext(),
            isFromGallery = true,
            isFromCamera = true,
            isAttachFile = false,
            isCropping = false,
            handler = object : PickerResultHandler() {
                override fun onSuccess(bitmap: Bitmap?, file: File?, fileName: String) {
                    viewModel.mutableProfileImage.value = bitmap
                    binding.ivProfile.setImageBitmap(viewModel.mutableProfileImage.value)
                    Log.d("MYValue", "onSuccess: B::$bitmap F::$file  FNAME::$fileName")
                }
                override fun onFailure(message: String) {
                    Log.d("MYValue","FAILURE:: $message")
                }
            })
    }

}
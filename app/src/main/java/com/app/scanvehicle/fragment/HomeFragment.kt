package com.app.scanvehicle.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.app.scanvehicle.BaseViewBindingFragment
import com.app.scanvehicle.R
import com.app.scanvehicle.adapter.VehiclesAdapter
import com.app.scanvehicle.api.NetworkResult
import com.app.scanvehicle.databinding.FragmentHomeBinding
import com.app.scanvehicle.model.VehicleDataItem
import com.app.scanvehicle.util.Constants
import com.app.scanvehicle.util.SharedPreference
import com.app.scanvehicle.util.changeBackground
import com.app.scanvehicle.util.showToast
import com.app.scanvehicle.viewModel.VehicleViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : BaseViewBindingFragment<FragmentHomeBinding>(), View.OnClickListener {

    lateinit var binding: FragmentHomeBinding
    private val viewModel: VehicleViewModel by viewModels()
    private var generalSetting: String = ""
    private var vehicleData = ArrayList<VehicleDataItem>()
    private var vehicle4WheelerData = ArrayList<VehicleDataItem>()
    private var vehicle2WheelerData = ArrayList<VehicleDataItem>()
    private lateinit var adapterCar: VehiclesAdapter
    private lateinit var adapterBike: VehiclesAdapter

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentHomeBinding) {
        binding = viewBinding
        binding.clickListener = this
        binding.tvTotalVehicle.text = getString(R.string.str_car_user_total, 0)
        callVehicleApi()
        findNavController().addOnDestinationChangedListener { _, destination, _ ->
            if (activity != null)
                activityContext().onDestinationChanged(destination)
        }
    }

    private fun selectedItem(isCarSelected: Boolean, isBikeSelected: Boolean) {
        if (isCarSelected) {
            binding.llCar.changeBackground(true)
            binding.llBike.changeBackground(false)
        }
        if (isBikeSelected) {
            binding.llBike.changeBackground(true)
            binding.llCar.changeBackground(false)
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentHomeBinding
        get() = FragmentHomeBinding::inflate

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.llCar -> {
                set4WheelerData(vehicleData)
                selectedItem(true, isBikeSelected = false)
            }

            R.id.llBike -> {
                set2WheelerData(vehicleData)
                selectedItem(false, isBikeSelected = true)
            }

            R.id.llAdd -> {
                findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAddVehicleFragment())
            }
        }
    }

    private fun callVehicleApi() {
        viewModel.getVehicleResponse.observe(this) {
            when (it) {
                is NetworkResult.Connection -> {
                    val data = SharedPreference.getValue(Constants.GENERAL_SETTING, "")
                    if (data == "") {
                        showNoInternetDialog { viewModel.getVehicleData() }
                    } else {
                        generalSetting = data.toString()
                        getVehicleFromDB()
                    }
                }

                is NetworkResult.Error -> {
                    requireContext().showToast(it.errorMessage)
                    loader.hide()
                    Log.d("MyValue", "Error Message:: ${it.errorMessage}")
                }

                is NetworkResult.Loading -> {
                    loader.showDialog(requireActivity())
                    Log.d("MyValue", "Loading...")
                }

                is NetworkResult.Success -> {

                    generalSetting = it.data.general_setting.show_person_name
                    vehicleData.clear()
                    vehicleData.addAll(it.data.vehicle_data)
                    loader.hide()
                    set4WheelerData(vehicleData)
                    Log.d("MyValue", "Success :: ${it.data}")
                }
            }
        }
    }

    private fun set4WheelerData(vehicleData: ArrayList<VehicleDataItem>) {
        vehicle4WheelerData = arrayListOf()
        adapterCar = VehiclesAdapter()

        vehicleData.forEach {
            if (it.vehicle_type == getString(R.string.type_4_wheeler))
                vehicle4WheelerData.addAll(listOf(it))
            binding.rvVehicleList.adapter = adapterCar
        }
        binding.tvTotalVehicle.text =
            getString(R.string.str_car_user_total, vehicle4WheelerData.size)
        adapterCar.submitList(vehicle4WheelerData)
    }

    private fun set2WheelerData(vehicleData: ArrayList<VehicleDataItem>) {
        vehicle2WheelerData = arrayListOf()
        adapterBike = VehiclesAdapter()
        vehicleData.forEach {
            if (it.vehicle_type == getString(R.string.type_2_wheeler))
                vehicle2WheelerData.addAll(listOf(it))
            binding.rvVehicleList.adapter = adapterBike
        }
        binding.tvTotalVehicle.text =
            getString(R.string.str_car_user_total, vehicle2WheelerData.size)
        adapterBike.submitList(vehicle2WheelerData)
    }

    private fun getVehicleFromDB() {
        vehicleData.clear()
        lifecycleScope.launch {
            viewModel.getVehicleFromDB().forEach {
                vehicleData.addAll(listOf(it))
                set4WheelerData(vehicleData)
            }
        }
    }
}


/**
 * val json = SharedPreference.getValue(Constants.VEHICLE_DATA_LIST, "")
 * val data = genericsFun.getFromJson<VehicleData>(json.toString())
 * if (data == null) {
 * showNoInternetDialog { viewModel.getVehicleData() }
 * } else {
 * generalSetting = data.general_setting.show_person_name
 * vehicleData.addAll(data.vehicle_data)
 * set4WheelerData(vehicleData)
 * } */

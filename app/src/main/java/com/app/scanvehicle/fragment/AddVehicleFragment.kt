package com.app.scanvehicle.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.app.scanvehicle.BaseViewBindingFragment
import com.app.scanvehicle.R
import com.app.scanvehicle.databinding.FragmentAddVehicleBinding
import com.app.scanvehicle.model.SpinnerModel
import com.app.scanvehicle.util.autoCleared
import com.app.scanvehicle.viewModel.AddVehicleViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddVehicleFragment : BaseViewBindingFragment<FragmentAddVehicleBinding>(),
    View.OnClickListener {

    private val viewModel: AddVehicleViewModel by viewModels()

    var binding: FragmentAddVehicleBinding by autoCleared()
    private lateinit var vehicleTypes: ArrayList<SpinnerModel>

    override fun onViewCreated(instance: Bundle?, viewBinding: FragmentAddVehicleBinding) {
        binding = viewBinding
        binding.clickListener = this
        binding.lifecycleOwner = this
//        binding.viewModelBinding = viewModel

        setXLSSpinner()
        setVehicleTypeSpinner()
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentAddVehicleBinding
        get() = FragmentAddVehicleBinding::inflate

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackBtn -> {
                findNavController().popBackStack()
            }

            R.id.btnCancel -> {
                findNavController().popBackStack()
            }

            R.id.btnSave -> {
                if (isValidate()) {
                    val vehicleType = vehicleTypes.first { e ->
                        (e.id == binding.spinnerVehicleType.selectedId.get())
                    }.name
                    Toast.makeText(requireContext(), "Selected Vehicle:: $vehicleType", Toast.LENGTH_SHORT).show()
                    Toast.makeText(requireContext(), "Add Vehicle successfully", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setXLSSpinner() {
        val xlsList = resources.getStringArray(R.array.XLSList)
        val adapter = ArrayAdapter(requireContext(),
            android.R.layout.simple_list_item_1, xlsList)

        binding.spinnerXls.adapter = adapter
        binding.spinnerXls.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                binding.spinnerXls.setSelection(0)
                val item = parent!!.getItemAtPosition(position).toString()
                if (position != 0)
                Toast.makeText(requireContext(), "Selected: $item", Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun setVehicleTypeSpinner() {
        vehicleTypes = arrayListOf(
            SpinnerModel(1, "2 Wheeler"),
            SpinnerModel(2, "4 Wheeler")
        )

        val vehicleTypeList: ArrayList<SpinnerModel> = arrayListOf()
        vehicleTypeList.add(SpinnerModel(0, "Choose vehicle type"))
        vehicleTypes.forEach {
            vehicleTypeList.add(SpinnerModel(it.id, it.name))
        }
        binding.spinnerVehicleType.setSpinnerItems(vehicleTypeList, 0)
    }

    private fun isValidate(): Boolean {
        var isValid = true
        if (!binding.etPersonName.isValidate())
            isValid = false
        if (!binding.etVehicleNo.isValidate())
            isValid = false
        if (!binding.spinnerVehicleType.isValidate())
            isValid = false
        return isValid
    }
}
package com.app.scanvehicle.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.app.scanvehicle.R
import com.app.scanvehicle.databinding.FragmentChangePasswordDialogBinding
import com.app.scanvehicle.util.autoCleared
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ChangePasswordDialogFragment : BottomSheetDialogFragment() {

    var binding : FragmentChangePasswordDialogBinding by autoCleared()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentChangePasswordDialogBinding.inflate(inflater,container, false)
        onEyePasswordClick()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnChangePassword.setOnClickListener {
            dismiss()
        }
    }

    private fun onEyePasswordClick() {
        binding.etCurrentPassword.passwordEyeVisible()
        binding.etCurrentPassword.passwordEyeClick()
        binding.etNewPassword.passwordEyeVisible()
        binding.etNewPassword.passwordEyeClick()
    }


}
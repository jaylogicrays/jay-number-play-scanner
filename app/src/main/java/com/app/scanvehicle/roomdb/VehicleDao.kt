package com.app.scanvehicle.roomdb

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.app.scanvehicle.model.VehicleDataItem

@Dao
interface VehicleDao {

    @Insert
    fun insertVehicleDataItem(vehicleItem: List<VehicleDataItem>)

    @Query("SELECT * FROM vehicle_data_table")
    fun getVehicleDataItem(): List<VehicleDataItem>

    @Query("DELETE FROM vehicle_data_table")
    fun deleteAllVehicleData()

}
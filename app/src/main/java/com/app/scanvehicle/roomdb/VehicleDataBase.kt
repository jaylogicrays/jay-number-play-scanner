package com.app.scanvehicle.roomdb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.app.scanvehicle.model.VehicleDataItem
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Database(entities = [VehicleDataItem::class], version = 3, exportSchema = false)
abstract class VehicleDataBase : RoomDatabase() {
    abstract fun vehicleDao(): VehicleDao

    @Module
    @InstallIn(SingletonComponent::class)
    object AppModule {

        @Singleton
        @Provides
        fun providerVehicleBD(@ApplicationContext app: Context) =
            Room.databaseBuilder(
                app,
                VehicleDataBase::class.java, "vehicle_db"
            ).allowMainThreadQueries().build()

        @Singleton
        @Provides
        fun providerVehicleDao(db: VehicleDataBase) = db.vehicleDao()
    }

}



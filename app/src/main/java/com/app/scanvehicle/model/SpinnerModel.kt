package com.app.scanvehicle.model

data class SpinnerModel(val id: Int, val name: String)
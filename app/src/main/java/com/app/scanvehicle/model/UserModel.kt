package com.app.scanvehicle.model

data class UserModel(
    val admin_email: String,
    val admin_level: String,
    val admin_name: String,
    val sid: String,

    val response : Boolean,
    val msg : String
)
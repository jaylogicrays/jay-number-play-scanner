package com.app.scanvehicle.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

data class VehicleData(
    val vehicle_data: List<VehicleDataItem>,
    val general_setting: GeneralSetting
)

@Entity(tableName = "vehicle_data_table")
data class VehicleDataItem(
    @PrimaryKey(autoGenerate = true)
    val id : Int = 0,
    val bld_city: String,
    val bld_email: String,
    val bld_mob: String,
    val bld_name: String,
    val bld_status: String,
    val company_email: String,
    val company_mob: String,
    val company_name: String,
    val company_passwd: String,
    val id_building: String,
    val id_company: String,
    val id_vehicle: String,
    val office_no: String,
    val person_name: String,
    val vehicle_no: String,
    val vehicle_type: String )

data class GeneralSetting(
    val show_person_name: String
)

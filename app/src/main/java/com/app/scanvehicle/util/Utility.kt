package com.app.scanvehicle.util

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.provider.Settings
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import com.app.scanvehicle.MainApp
import com.app.scanvehicle.R
import javax.inject.Inject

class Utility @Inject constructor() {

    companion object {
        private var instance: Utility? = null

        fun getInstance(): Utility {
            if (instance == null) {
                instance = Utility()
            }
            return instance as Utility
        }

        fun isPortraitMode(context: Context): Boolean =
            context.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
    }

    fun showNoInternetDialog(context: Context, tryAgainClick: View.OnClickListener) {
        val dialog = Dialog(context, R.style.AppTheme_Theme)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_no_internet, null)
        dialog.setContentView(view)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        view.findViewById<AppCompatButton>(R.id.btnTryAgain)
            .setOnClickListener {
                if (MainApp.getInstance().isConnectionAvailable()) {
                    dialog.dismiss()
                    tryAgainClick.onClick(it)
                }
            }
        dialog.show()
    }

    fun getPermissionForSDK33(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            Manifest.permission.READ_MEDIA_IMAGES
        } else {
            Manifest.permission.READ_EXTERNAL_STORAGE
        }
    }

    @SuppressLint("HardwareIds")
    //get device token for different devices
    fun getDeviceToken(context: Context?): String {
        return Settings.Secure.getString(context!!.contentResolver, Settings.Secure.ANDROID_ID)
    }

}
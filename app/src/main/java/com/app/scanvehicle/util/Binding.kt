package com.app.scanvehicle.util

import android.annotation.SuppressLint
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import com.app.scanvehicle.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

@SuppressLint("SetTextI18n")
@BindingAdapter("setVehicleNo")
fun TextView.setVehicleNo(text: String) {
    this.text = "Vehicle No: $text"
}

@SuppressLint("SetTextI18n")
@BindingAdapter("setVehicleType")
fun TextView.setVehicleType(text: String) {
    this.text = "Vehicle Type: $text"
}

@SuppressLint("SetTextI18n")
@BindingAdapter("setCompanyName")
fun TextView.setCompanyName(text: String) {
    this.text = "Company Name: $text"
}

@SuppressLint("SetTextI18n")
@BindingAdapter("setOfficeNo")
fun TextView.setOfficeNo(text: String) {
    this.text = "Office No: $text"
}

@BindingAdapter("setVisibilityOnAdminLevel")
fun View.setVisibilityOnAdminLevel(isHide: Boolean) {
    if (isHide) {
        this.hideView()
    } else {
        this.showView()
    }
}

@BindingAdapter("setImage")
fun AppCompatImageView.setImage(url: String) {
    if (url.isNotEmpty()) {
        Glide.with(this).load(url)
            .placeholder(R.drawable.ic_launcher_foreground).into(this)
    } else {
        setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_launcher_foreground,
                null
            )
        )
    }
}

@BindingAdapter("setImage", "imageRadius")
fun AppCompatImageView.setCropImage(url: String?, radius: Int?) {
    if (!url.isNullOrEmpty()) {
        Glide.with(this).asBitmap().load(url).centerCrop()
            .transform(RoundedCorners(radius!!))
            .placeholder(R.drawable.ic_launcher_foreground).into(this)
    } else {
        setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_launcher_foreground,
                null
            )
        )
    }
}
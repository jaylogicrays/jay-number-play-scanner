package com.app.scanvehicle.util

import com.google.gson.Gson
import javax.inject.Inject

class Generics @Inject constructor() {

    inline fun <reified T> getDataFromSharedPreferences(key: String): T? {

        val gson = Gson()
        val json = SharedPreference.getValue(key, "")
        return if (json != null) {
            gson.fromJson(json.toString(), T::class.java)
        } else {
            null
        }
    }

    inline fun <reified T> saveDataToSharedPreferences(key: String, data: T) {
        val gson = Gson()
        val json = gson.toJson(data)
        SharedPreference.setValue(key, json)
    }

    // Use for String to Json/data class convertor
    inline fun <reified T> getFromJson(json: String): T? {
        return if (json != null) {
            Gson().fromJson(json, T::class.java)
        } else null
    }

    // Use for Json/data class to String convertor
    inline fun <reified T> setToJson(data: T): String {
        return Gson().toJson(data)
    }

    inline fun <reified T> Gson.fromJson(json: String) : T =
        this.fromJson<T>(json, T::class.java)

}
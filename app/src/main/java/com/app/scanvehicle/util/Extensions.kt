package com.app.scanvehicle.util

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.app.scanvehicle.R
import com.app.scanvehicle.permission.PermissionCheck
import com.app.scanvehicle.permission.PermissionHandler
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

fun View?.showView() {
    this?.visibility = View.VISIBLE
}

fun View?.hideView() {
    this?.visibility = View.GONE
}

fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun View?.makeViewInvisible() {
    this?.visibility = View.INVISIBLE
}

fun View.changeBackground(isSelected : Boolean) {
    this.background =  if (isSelected)
        ContextCompat.getDrawable(this.context, R.drawable.bg_solid_yellow_radius15)
    else
        ContextCompat.getDrawable(this.context, R.drawable.bg_solid_white_gray_stock_radius15)
}

fun View.setupScanFrame(surfaceWidth: Int, surfaceHeight: Int): Rect {
    // Calculate the scan frame based on the position and size of the overlay view
    val overlayWidth = this.width
    val overlayHeight = this.height
    val overlayX = this.x
    val overlayY = this.y

    val left = (overlayX / surfaceWidth * 100).toInt()
    val top = (overlayY / surfaceHeight * 100).toInt()
    val right = ((overlayX + overlayWidth) / surfaceWidth * 100).toInt()
    val bottom = ((overlayY + overlayHeight) / surfaceHeight * 100).toInt()

    return Rect(left, top, right, bottom)
}

/**
 *  gives callbacks whenever any changes are recorded in the given EditText
 */
fun EditText.textChangeListener(onTextChange: (s: CharSequence?) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChange.invoke(s)
        }

    })
}

fun hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = activity.currentFocus
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * Get run time Permission Camera
 */
fun Context.checkPermissionReadWrite(onGranted: () -> Unit = {}) {
        PermissionCheck.check(this,
            arrayListOf(Manifest.permission.CAMERA),
            "Storage permission is necessary to store a image",

            object : PermissionHandler() {
                override fun onGranted() {
                    onGranted.invoke()
                }

                override fun onDenied(
                    context: Context,
                    deniedPermissions: java.util.ArrayList<String>
                ) {
                    super.onPermissionDenied(context, deniedPermissions)
                }
            }
        )
}

fun Context.dialContactPhone(phoneNumber: String) {
    val callIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
    callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    startActivity(callIntent)
}

fun Activity.setStatusBarTransparent() {
    window.apply {
        clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        } else {
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        statusBarColor = Color.TRANSPARENT
    }
}

fun Context.checkReadExternalPermission(onGranted: () -> Unit = {}) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            PermissionCheck.check(this, arrayListOf(  Manifest.permission.CAMERA,
                Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO),
                "",
                handler= object : PermissionHandler() {
                    override fun onGranted() {
                        onGranted.invoke()
                    }

                    override fun onDenied(
                        context: Context,
                        deniedPermissions: java.util.ArrayList<String>
                    ) {
                        super.onPermissionDenied(context, deniedPermissions)
                    }
                }
            )
        } else {
            PermissionCheck.check(this,
                arrayListOf( Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE ),
//                "Storage permission is necessary to store a image",
                "",
                handler = object : PermissionHandler() {
                    override fun onGranted() {
                        onGranted.invoke()
                    }

                    override fun onDenied(
                        context: Context,
                        deniedPermissions: java.util.ArrayList<String>
                    ) {
                        super.onPermissionDenied(context, deniedPermissions)
                    }
                }
            )
        }
    }

fun Activity.alertDialog(title : String, msg: String, positiveClick: () -> Unit) {

    val dialog = AlertDialog.Builder(this)
    dialog.setMessage(msg)
    dialog.setTitle(title)
    dialog.setCancelable(false)
    dialog.setPositiveButton("Yes") { dialog, _ ->
        positiveClick.invoke()
        dialog.dismiss()
    }
    dialog.setNegativeButton("No") {dialog,_ ->
        dialog.cancel()
    }
    val alertDialog = dialog.create()
    alertDialog.show()
}


fun ImageView.loadImageWithCustomCorners(@DrawableRes resId: Int, radius: Int) =
    Glide.with(this)
        .load(resId)
        .transform(CenterCrop(), RoundedCorners(radius))
        .into(this)
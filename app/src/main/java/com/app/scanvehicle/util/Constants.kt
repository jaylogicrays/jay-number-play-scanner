package com.app.scanvehicle.util

object Constants {
    const val VEHICLE_DATA_LIST = "Vehicle Data List"
    const val USER_INFO = "User info"
    const val USER_EMAIL = "User email"
    const val ADMIN_LEVEL = "ADMIN_LEVEL"
    const val GENERAL_SETTING = "generalSetting"

    //Global Retrofit options
    const val CONNECTION_TIMEOUT = 10L
    const val READ_TIMEOUT = 10L
    const val WRITE_TIMEOUT = 10L

    //bundle Keys
    const val IS_OFFICE_USER = "A"
    const val IS_BUILDER_USER = "B"
    const val IS_WATCH_MAN_USER = "C"
}


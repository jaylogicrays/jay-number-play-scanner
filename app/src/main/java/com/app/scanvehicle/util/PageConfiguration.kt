package com.app.scanvehicle.util

import androidx.annotation.IdRes
import com.app.scanvehicle.R

enum class PageConfiguration(
    val id: Int,
    val bottomNavigationBarVisible: Boolean = false
) {

    HOME(
        R.id.homeFragment,
        bottomNavigationBarVisible = true
    ),
    SCAN(
        R.id.scanNumberFragment,
        bottomNavigationBarVisible = true
    ),
    PROFILE(
        R.id.profileFragment,
        bottomNavigationBarVisible = true
    ),
    COMPANY_INFO(
        R.id.companyDetailsFragment,
        bottomNavigationBarVisible = true
    ),
    OTHER(0);

    companion object {
        fun gerConfiguration(@IdRes id: Int): PageConfiguration {
            return values().firstOrNull { it.id == id } ?: OTHER
        }
    }

}